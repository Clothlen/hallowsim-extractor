# Contributing

Avoid using the name "Hollow Knight" or "Silksong" in the source code or
resources. Instead, try to call it "vanilla," "original," or something similar.
