namespace HallowsimExtractor.extraction {

	// TODO: may be better to be a struct.
	internal class Metadata
	{

		internal readonly string guid;

		internal readonly string path;

		internal readonly string child_path;

		internal Metadata(string guid, string path, string child) {
			this.guid = guid;
			this.path = path;
			this.child_path = child;
		}
	}
}
