namespace HallowsimExtractor.extraction {
	using System;
	using System.Collections;
	using System.Collections.Concurrent;
	using System.Threading;
	using System.Threading.Tasks;
	using System.Collections.Generic;
	using System.Collections.Immutable;
	using Godot;

	internal static class AssetExtractor
	{

		internal const string ASSETS_DIR = "user://globalgamemanagers/Assets";

		internal const string OUTPUT_DIR = "user://vanilla";

		internal static string assets_dir;

		internal static string output_dir;

		internal static directoryhandler.AbstractDirectoryHandler[] directory_handlers = new directoryhandler.AbstractDirectoryHandler[] {
			new directoryhandler.AnimationClipDirectoryHandler(),
			new directoryhandler.AnimatorControllerDirectoryHandler(),
			new directoryhandler.AnimatorOverrideControllerDirectoryHandler(),
			new directoryhandler.AudioClipDirectoryHandler(),
			new directoryhandler.FontDirectoryHandler(),
			new directoryhandler.MaterialDirectoryHandler(),
			new directoryhandler.MeshDirectoryHandler(),
			new directoryhandler.PhysicsMaterial2DDirectoryHandler(),
			new directoryhandler.PrefabInstanceDirectoryHandler(),
			new directoryhandler.ResourcesDirectoryHandler(),
			new directoryhandler.SceneDirectoryHandler(),
			new directoryhandler.ScriptableObjectDirectoryHandler(),
			new directoryhandler.ScriptsDirectoryHandler(),
			new directoryhandler.ShaderDirectoryHandler(),
			new directoryhandler.TextAssetDirectoryHandler(),
			new directoryhandler.Texture2DDirectoryHandler()
		};

		internal static ImmutableDictionary<string, Metadata> guids;

		internal static ImmutableDictionary<string, string> path_to_guid_meta_map;

		private static int _progress_resets = 1;

		private static int _progress = 0;

		private static int _max_progress = 0;

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		static AssetExtractor() {
			assets_dir = ProjectSettings.GlobalizePath(ASSETS_DIR);
			output_dir = ProjectSettings.GlobalizePath(OUTPUT_DIR);
		}

		internal static async void go() {
			log.Info("Going.");


			if (!util.GodotDirUtil.dir_exists(assets_dir)) {
				log.Error("No assets dir detected.");
				return;
			}

			var dirs = util.GodotDirUtil.list_only_dirs(assets_dir);

			var all_actions = new Dictionary<string, IEnumerator<directoryhandler.AbstractDirectoryHandler.Act>>();

			// What we must first do,
			// is find all meta files.
			log.Info("Entering stage 1 (meta)...");
			log.Info("Finding .meta files...");
			await _handle_meta(dirs);
			log.Info("Found all .meta files.");
			log.Info("Finished stage 1 (meta).");

			// reset_progress();

			foreach (var dir in dirs) {
				var actions = _get_actions_for(dir);
				if (actions != null) {
					all_actions.Add(dir, actions);
				}
			}

			// Now actually do all of the actions.
			while (all_actions.Count > 0) {
				reset_progress();
				log.Info("Entering stage {0}...", _progress_resets);
				var remove_these_keys = new List<string>();
				foreach (var kv in all_actions) {
					var dir = kv.Key;
					var enumerator = kv.Value;

					var has_current = enumerator.MoveNext();
					if (has_current) {
						await enumerator.Current(dir);
					}
					else {
						remove_these_keys.Add(kv.Key);
					}
				}

				log.Info("Cleaning up stage {0}.", _progress_resets);

				foreach (var key in remove_these_keys) {
					all_actions.Remove(key);
				}

				log.Info("Finished stage {0}.", _progress_resets);
			}


			log.Info("DONE WITH ALL ACTIONS, FINISHED.");

		}

		internal static void increment_progress(int amount = 1) {
			// _progress += amount;
			Interlocked.Add(ref _progress, amount);
		}

		internal static void increment_max_progress(int amount = 1) {
			// _max_progress += amount;
			Interlocked.Add(ref _max_progress, amount);
		}

		internal static void reset_progress() {
			Interlocked.Add(ref _progress, -_progress);
			Interlocked.Add(ref _max_progress, -_max_progress);
			Interlocked.Add(ref _progress_resets, 1);
		}

		internal static (int, int, int) poll_progress() {
			return (_progress_resets, _progress, _max_progress);
		}

		private static IEnumerator<directoryhandler.AbstractDirectoryHandler.Act>? _get_actions_for(string dir) {
			directoryhandler.AbstractDirectoryHandler handler = null!;
			foreach (var directory_handler in directory_handlers) {
				if (directory_handler.can_handle(dir)) {
					handler = directory_handler;
					break;
				}
			}

			if (handler == null) {
				log.Warn($"No handler for: {dir}");
				return null;
			}

			var enumerator = handler.get_actions(dir);

			return enumerator;
		}

		private static async Task _handle_meta(ImmutableList<string> dirs) {

			var collection = new ConcurrentDictionary<string, Metadata>();
			var tasks = new List<Task>();
			foreach (var dir in dirs) {
				tasks.Add(new Task(
					() => _handle_meta_dir(dir, ref collection)
				));
			}
			foreach (var task in tasks) {
				task.Start();
			}
			await Task.WhenAll(tasks);

			AssetExtractor.guids = collection.ToImmutableDictionary();
			collection.Clear();

			var work_dictionary = new Dictionary<string, string>();
			foreach (var item in AssetExtractor.guids.Values) {
				work_dictionary.Add(item.path, item.guid);
			}
			AssetExtractor.path_to_guid_meta_map = work_dictionary.ToImmutableDictionary();

		}

		private static void _handle_meta_dir(string dir, ref ConcurrentDictionary<string, Metadata> collection) {
			increment_max_progress();
			// var yaml_deserializer = new YamlDotNet.Serialization.Deserializer();
			var meta_files = new List<string>(util.GodotDirUtil.list_only_files(dir));
			increment_max_progress(meta_files.Count);
			for (int i = meta_files.Count - 1; i >= 0; i--) {
				if (!meta_files[i].EndsWith(".meta")) {
					meta_files.RemoveAt(i);
				}
				increment_progress();
			}

			// Now we can handle each .meta file individually.
			increment_max_progress(meta_files.Count);
			foreach (var meta_file in meta_files) {
				string guid;
				using (var reader = new System.IO.StreamReader(meta_file)) {
					// Skip first line, which is always fileFormatVersion
					reader.ReadLine();
					guid = reader.ReadLine();
				}
				const string key = "guid: ";
				if (!guid.StartsWith(key)) {
					throw new ApplicationException($"Could not find GUID for meta file in directory {dir} at {meta_file}");
				}

				var guid_value = guid.Substring(key.Length, guid.Length - key.Length);

				const string file_end = ".meta";
				var child_path = meta_file.Substring(0, meta_file.Length - file_end.Length);

				if (!collection.TryAdd(
					guid_value,
					new Metadata(guid_value, meta_file, child_path)
				)) {
					throw new ApplicationException("GUID conflict?");
				}

				increment_progress();

			}

			increment_progress();
		}

	}
}
