namespace HallowsimExtractor.extraction.classes.texture2d {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
	internal class SpritesheetSprite
	{
		[JsonProperty("name")]
		internal string name;

		[JsonProperty("rect")]
		internal SpriteRect rect;

		[JsonProperty("outline")]
		internal Godot.Vector2[][] outline;

		[JsonProperty("pivot")]
		internal Godot.Vector2 pivot;

		[JsonProperty("physicsShape")]
		internal Godot.Vector2[][] physics_shape;

	}

	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
	internal class SpriteRect
	{
		[JsonProperty("x")]
		internal float x;

		[JsonProperty("y")]
		internal float y;

		[JsonProperty("width")]
		internal float width;

		[JsonProperty("height")]
		internal float height;

	}
}
