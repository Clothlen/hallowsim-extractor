namespace HallowsimExtractor.extraction.classes {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
	internal class FileReference
	{
		[JsonProperty("guid")]
		internal string guid;
	}
}
