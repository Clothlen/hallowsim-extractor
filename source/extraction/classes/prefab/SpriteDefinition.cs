namespace HallowsimExtractor.extraction.classes.prefab {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
	internal class SpriteDefinition
	{

		[JsonProperty("name", Required=Required.Default)]
		internal string name;

		[JsonProperty("uvs")]
		internal Godot.Vector2[] uvs;

		[JsonProperty("flipped")]
		internal int flipped;

		[JsonProperty("material")]
		internal FileReference material;

		[JsonProperty("positions")]
		internal Godot.Vector3[] positions;

		[JsonProperty("texelSize")]
		internal Godot.Vector2 texel_size;

	}
}
