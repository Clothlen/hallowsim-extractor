namespace HallowsimExtractor.extraction.classes.prefab {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.DisallowNull)]
	internal class ClipCollection
	{
		[JsonIgnore]
		internal string guid;

		[JsonProperty("clips")]
		internal Clip[] clips;
	}
}
