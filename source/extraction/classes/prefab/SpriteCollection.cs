namespace HallowsimExtractor.extraction.classes.prefab {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
	internal class SpriteCollection
	{
		[JsonIgnore]
		internal string guid;

		[JsonProperty("spriteDefinitions")]
		internal SpriteDefinition[] sprite_definitions;

		[JsonProperty("textures")]
		internal FileReference[] textures;

		[JsonProperty("spriteCollectionName")]
		internal string name;

		[JsonProperty("spriteCollectionGUID")]
		internal string internal_guid;

		[JsonProperty("materials")]
		internal FileReference[] materials;

	}
}
