namespace HallowsimExtractor.extraction.classes.prefab {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
	internal class ConvertedClip
	{

		[JsonProperty("name")]
		internal string name;

		[JsonProperty("frames")]
		internal string[] frames;

		[JsonProperty("fps")]
		internal float fps;

		[JsonProperty("loop_start")]
		internal int loop_start;

		[JsonProperty("wrap_mode")]
		internal int wrap_mode;

	}
}
