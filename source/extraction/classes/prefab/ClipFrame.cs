namespace HallowsimExtractor.extraction.classes.prefab {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
	internal class ClipFrame
	{
		[JsonProperty("spriteCollection")]
		internal FileReference sprite_collection;

		[JsonProperty("spriteId")]
		internal int sprite_id;
	}
}
