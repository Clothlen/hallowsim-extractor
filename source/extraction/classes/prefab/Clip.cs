namespace HallowsimExtractor.extraction.classes.prefab {
	using Newtonsoft.Json;

	[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.AllowNull)]
	internal class Clip
	{
		[JsonProperty("name")]
		internal string? name;

		[JsonProperty("frames")]
		internal ClipFrame[] frames;

		[JsonProperty("fps")]
		internal float fps;

		[JsonProperty("loopStart")]
		internal int loop_start;

		[JsonProperty("wrapMode")]
		internal int wrap_mode;
	}
}
