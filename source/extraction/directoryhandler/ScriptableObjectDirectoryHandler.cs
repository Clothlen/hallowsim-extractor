namespace HallowsimExtractor.extraction.directoryhandler {
	internal class ScriptableObjectDirectoryHandler : util.IgnoreHandler
	{
		internal override string name { get { return "ScriptableObject"; } }
	}
}
