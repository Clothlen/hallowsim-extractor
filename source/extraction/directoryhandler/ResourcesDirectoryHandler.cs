namespace HallowsimExtractor.extraction.directoryhandler {
	internal class ResourcesDirectoryHandler : util.IgnoreHandler
	{
		internal override string name { get { return "Resources"; } }
	}
}
