namespace HallowsimExtractor.extraction.directoryhandler {
	internal class ShaderDirectoryHandler : util.IgnoreHandler
	{
		internal override string name { get { return "Shader"; } }
	}
}
