namespace HallowsimExtractor.extraction.directoryhandler {
	internal class ScriptsDirectoryHandler : util.IgnoreHandler
	{
		internal override string name { get { return "Scripts"; } }
	}
}
