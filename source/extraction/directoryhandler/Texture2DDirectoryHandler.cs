namespace HallowsimExtractor.extraction.directoryhandler {
	using System;
	using System.Diagnostics;
	using System.Threading.Tasks;
	using System.Collections.Generic;
	using Godot;

	internal class Texture2DDirectoryHandler : AbstractDirectoryHandler
	{

		internal override string name { get { return "Texture2D"; } }

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		internal override IEnumerator<Act> get_actions(string directory) {
			yield return this._first;
		}

		private async Task _first(string directory) {
			var files = HallowsimExtractor.util.GodotDirUtil.list_only_files(directory);

			AssetExtractor.increment_max_progress(files.Count);
			log.Info("Handling {0} textures. This may take a while.", files.Count);

			var tasks = new List<Task>();
			foreach (var file in files) {
				tasks.Add(new Task(() => this._handle_file(file)));
			}

			foreach (var task in tasks) {
				task.Start();
			}
			await Task.WhenAll(tasks.ToArray());
			tasks.Clear();

			log.Info("Finished handling textures.");

		}

		private void _handle_file(string file) {
			if (file.EndsWith(".meta")) {
				return;
			}

			// Disabled for now. Very spammy, doesn't tell us much.
			// log.Info("Handling texture {0}", System.IO.Path.GetFileNameWithoutExtension(file));

			var metafile = $"{file}.meta";
			// Open the meta file and see what we are dealing with.
			var json_obj = this._read_meta_file(metafile);

			if (json_obj.Property("TextureImporter") == null) {
				throw new ApplicationException($"Texture2D does not have a TextureImporter property at `{metafile}`.");
			}

			var texture_importer = (Newtonsoft.Json.Linq.JObject)json_obj.GetValue("TextureImporter")!;
			if (texture_importer.Property("spriteSheet") == null) {
				throw new ApplicationException($"Texture2D's TextureImporter does not have a spriteSheet property at `{metafile}`.");
			}

			var sprite_sheet = (Newtonsoft.Json.Linq.JObject)texture_importer.GetValue("spriteSheet")!;
			if (sprite_sheet.Property("sprites") == null) {
				throw new ApplicationException($"Texture2D's sprite sheet does not have a sprites property at `{metafile}`.");
			}

			var sprites = (Newtonsoft.Json.Linq.JArray)sprite_sheet.GetValue("sprites")!;

			// TODO: this is terrible! but too bad
			// TODO: we can use TextureImporter.spriteMode instead
			if (sprites.Count > 1) {
				// It's a sprite sheet.
				// Explode!

				// Something noticed: in vanilla, all of the known spritesheets always follow this format:
				// SpriteAtlasTexture-<name>-<size>-fmt2
				// The size is always formatted like 1234x1234.
				// The name can be anything.
				// Let's figure it out.
				var spritesheet_name = System.IO.Path.GetFileNameWithoutExtension(file);
				var splitted_spritesheet_name = spritesheet_name.Split(new char[] { '-' });
				Debug.Assert(splitted_spritesheet_name.Length == 4);
				var real_spritesheet_name = splitted_spritesheet_name[1];

				// does the real name exist in collections?
				// This is confirmed to *only* happen with Grub
				if (real_spritesheet_name == "Grub") {
					real_spritesheet_name = "Grub_sheet";
				}

				var dir = System.IO.Path.Combine(AssetExtractor.output_dir, "images", "collections", real_spritesheet_name);
				System.IO.Directory.CreateDirectory(dir);

				var parent_image = this._load_image(file);
				var parent_image_size = parent_image.GetSize();

				foreach (var s in sprites) {
					var sprite = s.ToObject<classes.texture2d.SpritesheetSprite>();
					if (sprite == null) {
						throw new ApplicationException($"Could not get sprite from spritesheet from file {metafile}");
					}

					// Go ahead and extract it from the parent image.
					var sprite_image = parent_image.GetRect(new Rect2(
						sprite.rect.x,
						parent_image_size.y - sprite.rect.y - sprite.rect.height,
						sprite.rect.width,
						sprite.rect.height
					));


					var path = System.IO.Path.Combine(dir, $"{sprite.name}.png");
					System.IO.File.WriteAllBytes(
						path,
						sprite_image.SavePngToBuffer()
					);

					// this._cut_image_polygon(sprite, path, sprite.outline[0]);
					// this._cut_image_polygon2(sprite, path, sprite.physics_shape);

					// Cut it up from its spritesheet.
					this._cut_image_polygon(sprite, path);

				}

			}
			else if (sprites.Count == 1) {
				// It's a single sprite.
				// Just copy it over.
				var dir = System.IO.Path.Combine(AssetExtractor.output_dir, "images", "sprites");
				System.IO.Directory.CreateDirectory(dir);  // TODO: we should probably only run this once.
				var path = System.IO.Path.Combine(dir, System.IO.Path.GetFileName(file));
				#if DEBUG
					// Overwrite in debug.
					System.IO.File.Copy(file, path, true);
				#else
					System.IO.File.Copy(file, path);
				#endif
			}
			else if (sprites.Count == 0) {

			}
			else {
				throw new ApplicationException($"Texture2D has a negative amount of sprites? (at {metafile})");
			}


			AssetExtractor.increment_progress();

		}

		private Newtonsoft.Json.Linq.JObject _read_meta_file(string path) {
			var text = System.IO.File.ReadAllText(path);
			var yaml_obj = new YamlDotNet.Serialization.Deserializer().Deserialize<object>(text);
			var json_text = Newtonsoft.Json.JsonConvert.SerializeObject(yaml_obj);
			return Newtonsoft.Json.Linq.JObject.Parse(json_text);
		}

		private Image _load_image(string path) {
			var image = new Image();

			image.LoadPngFromBuffer(System.IO.File.ReadAllBytes(path));

			return image;
		}

		// TODO: This seems to mostly work, but there are some problems.
		// The polygon given by the sprite outline does not always crop correctly.
		// See:
		// * SpriteAtlasTexture-Bretta_Basement-2048x2048-fmt12
		// ** Mighty_Zote_0016_6
		// *** Note how the inside of the polygon *should* be cut, but isn't.
		// *** Instead, the *outside* is cut; not desirable!
		// *** Inverting it here messes with other sprites that do work
		// *** correctly.
		// *** Could there be a setting inside it's MetaFile TextureImporter
		// *** that can tell us to invert it or not?
		// * SpriteAtlasTexture-GG_Main-4096x4096-fmt12
		// ** GG_Scenery_0017_4
		// *** Note how there should be two rocks, but one is gone.
		// *** But it shows up if we use sprite.physics_shape instead, but then
		// *** the other disappears instead.
		// ** GG_UI_pieces_0007_I2
		// *** Note how the left half is gone if we use sprite.outline and the
		// *** right half is visible.
		// *** When we use sprite.physics_shape, this reverses again.
		private void _cut_image_polygon(classes.texture2d.SpritesheetSprite sprite, string path) {
			var polygons = sprite.outline;

			if (polygons.Length < 1) {
				log.Error($"No polygons in sprite {sprite.name}");
				return;
			}

			// We can't use `using` here because we cannot save `bitmap_new` to
			// a file while `bitmap_old` exists.
			var bitmap_new = new System.Drawing.Bitmap((int)sprite.rect.width, (int)sprite.rect.height);

			using (var graphics_path = new System.Drawing.Drawing2D.GraphicsPath())
			// Yes..., we save the PNG to disk and then reload it.
			// Unfortunately there doesn't seem to be a way to
			// convert Godot.Image to System.Drawing.Bitmap.
			// TODO: maybe we just don't even use Godot.Image?
			using (var bitmap_old = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromFile(path))
			using (var graphics = System.Drawing.Graphics.FromImage(bitmap_new))
			{
				// For now, we'll just use the first polygon.
				// But there are multiple polygons; how to handle?
				var tmppoly = new System.Drawing.PointF[polygons[0].Length];
				for (int i = 0; i < polygons[0].Length; i++) {
					tmppoly[i] = new System.Drawing.PointF(
						polygons[0][i].x + sprite.rect.width * sprite.pivot.x,
						// Need to flip the Y.
						sprite.rect.height - (polygons[0][i].y + sprite.rect.height * sprite.pivot.y)
					);
				}

				graphics_path.AddPolygon(tmppoly);

				// Uncomment this to see the polygon borders.
				// for (int i = 0; i < polygons.Length; i++) {
				// 	var polygon = polygons[i];
				// 	var poly = new System.Drawing.PointF[polygon.Length];
				// 	for (int j = 0; j < polygon.Length; j++) {
				// 		poly[j] = new System.Drawing.PointF(
				// 			polygon[j].x + sprite.rect.width * sprite.pivot.x,
				// 			// It's upside down!
				// 			sprite.rect.height - (polygon[j].y + sprite.rect.height * sprite.pivot.y)
				// 		);
				// 	}
				// 	var color = System.Drawing.Color.MediumSeaGreen;
				// 	if (i > 1) {
				// 		color = System.Drawing.Color.MediumVioletRed;
				// 	}

				// 	graphics.DrawPolygon(
				// 		new System.Drawing.Pen(color, i),
				// 		poly
				// 	);
				// }

				graphics.Clip = new System.Drawing.Region(graphics_path);
				graphics.DrawImage(bitmap_old, 0, 0);

				// We could possibly use this to reverse it.
				// Unfortunately it doesn't work.
				// using (var brush = new System.Drawing.SolidBrush(System.Drawing.Color.Transparent)) {
				// 	graphics.FillPolygon(brush, tmppoly);
				// }

			}

			bitmap_new.Save(path, System.Drawing.Imaging.ImageFormat.Png);
			bitmap_new.Dispose();
		}

	}
}
