namespace HallowsimExtractor.extraction.directoryhandler {
	internal class TextAssetDirectoryHandler : util.IgnoreHandler
	{
		internal override string name { get { return "TextAsset"; } }
	}
}
