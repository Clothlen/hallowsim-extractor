namespace HallowsimExtractor.extraction.directoryhandler {
	internal class AnimatorOverrideControllerDirectoryHandler : util.IgnoreHandler  // what a long name
	{
		internal override string name { get { return "AnimatorOverrideController"; } }
	}
}
