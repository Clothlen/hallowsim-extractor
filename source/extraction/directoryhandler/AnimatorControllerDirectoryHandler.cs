namespace HallowsimExtractor.extraction.directoryhandler {
	internal class AnimatorControllerDirectoryHandler : util.IgnoreHandler
	{
		internal override string name { get { return "AnimatorController"; } }
	}
}
