namespace HallowsimExtractor.extraction.directoryhandler {
	internal class SceneDirectoryHandler : util.IgnoreHandler
	{
		internal override string name { get { return "Scene"; } }
	}
}
