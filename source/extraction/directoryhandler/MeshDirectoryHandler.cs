namespace HallowsimExtractor.extraction.directoryhandler {
	internal class MeshDirectoryHandler : util.IgnoreHandler
	{
		internal override string name { get { return "Mesh"; } }
	}
}
