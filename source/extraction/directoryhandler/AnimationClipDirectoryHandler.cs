namespace HallowsimExtractor.extraction.directoryhandler {
	internal class AnimationClipDirectoryHandler : util.IgnoreHandler
	{
		internal override string name { get { return "AnimationClip"; } }
	}
}
