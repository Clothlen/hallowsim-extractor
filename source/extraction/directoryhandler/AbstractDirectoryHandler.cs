namespace HallowsimExtractor.extraction.directoryhandler {
	using System.Collections.Generic;

	internal abstract class AbstractDirectoryHandler
	{

		internal abstract string name { get; }

		internal virtual bool can_handle(string directory) {
			return directory.EndsWith(this.name);
		}

		internal delegate System.Threading.Tasks.Task Act(string directory);

		internal abstract IEnumerator<Act> get_actions(string directory);
	}
}
