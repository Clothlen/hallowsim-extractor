namespace HallowsimExtractor.extraction.directoryhandler {
	internal class FontDirectoryHandler : util.CopyHandler
	{

		internal override string name { get { return "Font"; } }

		internal override string destination_name { get { return "font"; } }

	}
}
