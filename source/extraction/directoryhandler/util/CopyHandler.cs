namespace HallowsimExtractor.extraction.directoryhandler.util {

	using System.IO;
	using System.Collections.Generic;
	using System.Threading.Tasks;

	internal abstract class CopyHandler : AbstractDirectoryHandler
	{

		internal abstract string destination_name { get; }

		internal override IEnumerator<Act> get_actions(string directory) {
			yield return this._first;
		}

		private async Task _first(string directory) {
			var files = HallowsimExtractor.util.GodotDirUtil.list_only_files(directory);

			var dir = Path.Combine(AssetExtractor.output_dir, this.destination_name);
			Directory.CreateDirectory(dir);

			var tasks = new List<Task>();
			foreach (var file in files) {
				if (file.EndsWith(".meta")) {
					continue;
				}

				var dest = Path.GetFileName(file);
				dest = Path.Combine(dir, dest);

				tasks.Add(new Task(
					() => {
						#if DEBUG
							File.Copy(file, dest, true);
						#else
							File.Copy(file, dest);
						#endif
						AssetExtractor.increment_progress();
					}
				));
			}

			AssetExtractor.increment_max_progress(tasks.Count);
			foreach (var t in tasks) {
				t.Start();
			}

			await Task.WhenAll(tasks.ToArray());

		}

	}
}
