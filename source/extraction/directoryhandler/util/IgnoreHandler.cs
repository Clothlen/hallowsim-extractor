namespace HallowsimExtractor.extraction.directoryhandler.util {
	using System.Collections.Generic;

	internal abstract class IgnoreHandler : AbstractDirectoryHandler
	{

		internal override IEnumerator<Act> get_actions(string directory) {
			yield break;
		}
	}
}
