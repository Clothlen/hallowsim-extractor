namespace HallowsimExtractor.extraction.directoryhandler {
	using System;
	using System.Collections.Immutable;
	using System.Collections.Concurrent;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Threading.Tasks;
	using Godot;

	internal class PrefabInstanceDirectoryHandler : AbstractDirectoryHandler
	{

		// private readonly List<classes.prefab.SpriteCollection> _sprite_collections = new List<classes.prefab.SpriteCollection>();

		// private readonly List<classes.prefab.ClipCollection> _clip_collections = new List<classes.prefab.ClipCollection>();

		// // Used for converting clip collections only.
		// private readonly Dictionary<string, int> _sprite_collection_guids = new Dictionary<string, int>();

		// // Clip collections have no identifiers. Thus we must make one ourselves.
		private readonly Dictionary<string, string> _clip_collection_guid_to_names = new Dictionary<string, string>();

		// key = guid
		private ImmutableDictionary<string, classes.prefab.SpriteCollection> _sprite_collections;

		// key = name, auto-generated from filenames
		private ImmutableDictionary<string, classes.prefab.ClipCollection> _clip_collections;

		// List of sprites that have negative UV regions.
		private readonly List<string> _negative_sprites = new List<string>();

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		internal override string name { get { return "PrefabInstance"; } }

		internal override IEnumerator<Act> get_actions(string directory) {
			// this._handle_first(directory);
			// yield return null;
			yield return this._first;
			yield return this._second;

			yield break;
		}

		private async Task _first(string directory) {
			var files = HallowsimExtractor.util.GodotDirUtil.list_only_files(directory);

			var tasks = new List<Task>();
			var sprite_collections = new ConcurrentDictionary<string, classes.prefab.SpriteCollection>();
			var clip_collections = new ConcurrentDictionary<string, classes.prefab.ClipCollection>();
			foreach (var file in files) {
				if (file.EndsWith(".meta")) {
					continue;
				}

				// TODO: These are massive files that do contain info we need
				// right now.
				// They might be needed later but due to their massive size
				// let's just skip them.
				if (file.EndsWith("Knight Get Scream.prefab") || file.EndsWith("_GameCameras.prefab")) {
					continue;
				}

				tasks.Add(
					new Task(
						() => {
							try {
								this._handle_file(file, sprite_collections, clip_collections);
							}
							catch (Exception e) {
								log.Error("An error occured when handling file {0}. Error: {1}", file, e);
								throw;
							}
						}
					)
				);
			}

			AssetExtractor.increment_max_progress(tasks.Count);

			foreach (var t in tasks) {
				t.Start();
			}

			await Task.WhenAll(tasks.ToArray());
			// Task.WaitAll(tasks.ToArray());

			this._sprite_collections = sprite_collections.ToImmutableDictionary();
			this._clip_collections = clip_collections.ToImmutableDictionary();

			foreach (var kv in this._clip_collections) {
				this._clip_collection_guid_to_names.Add(kv.Value.guid, kv.Key);
			}

		}

		private async Task _second(string directory) {
			log.Info("Handling {0} sprite collections.", this._sprite_collections.Count);
			AssetExtractor.increment_max_progress(this._sprite_collections.Count);

			var tasks = new List<Task>();
			foreach (var sprite_collection in this._sprite_collections.Values) {
				// this._handle_sprite_collection(sprite_collection);

				tasks.Add(new Task(() => this._handle_sprite_collection(sprite_collection)));
			}

			foreach (var task in tasks) {
				task.Start();
			}

			// await Task.Run(() => Parallel.ForEach(this._sprite_collections.Values, this._handle_sprite_collection));

			await Task.WhenAll(tasks.ToArray());

			log.Info("Done with sprite collections.");
			log.Info("Handling {0} clip collections.", this._clip_collections.Count);

			// Now do the clip collections.
			tasks.Clear();
			AssetExtractor.increment_max_progress(this._clip_collections.Count);
			foreach (var clip_collection in this._clip_collections.Values) {
				tasks.Add(new Task(() => this._handle_clip_collection(clip_collection)));
			}
			foreach (var task in tasks) {
				task.Start();
			}

			await Task.WhenAll(tasks.ToArray());

			log.Info("Done with clip collections.");

			log.Info("DONE with second.");
		}

		private void _handle_file(
			string file,
			ConcurrentDictionary<string, classes.prefab.SpriteCollection> sprite_collections,
			ConcurrentDictionary<string, classes.prefab.ClipCollection> clip_collections
		) {
			// AssetExtractor.increment_max_progress();

			// Very spammy, not really useful.
			// log.Info("Handling prefab {0}.", System.IO.Path.GetFileNameWithoutExtension(file));

			var yaml_text = YAMLHelper.clean_yaml(System.IO.File.ReadAllLines(file));

			var yaml_deserializer = new YamlDotNet.Serialization.Deserializer();
			var yaml_object = yaml_deserializer.Deserialize<object>(yaml_text);

			// Now we can convert it to JSON.
			var json_text = Newtonsoft.Json.JsonConvert.SerializeObject(yaml_object);
			// Now convert it to a token.
			var json_object = Newtonsoft.Json.Linq.JObject.Parse(json_text);

			// Now
			// Now we have a JSON object that we can easily interact with.
			// First! Simple validation.
			// It is known that all prefab files have at least these properties.
			foreach (var item in new[] {  // interestingly, some files such as MenuAchievement.prefab are very weird.
				"Prefab",
				"GameObject",
				// some don't have these
				// "Transform",
				// some have "SpriteRenderer" instead
				// see Breakable Pole Base.prefab
				// "MonoBehaviour"
			}) {
				if (json_object.Property(item) == null) {
					throw new ApplicationException($"Not a valid prefab file at {file}: Could not find required property {item}.");
				}
			}

			// // Now to figure out what exactly it is.

			this._handle_jobject(file, json_object, sprite_collections, clip_collections);

			AssetExtractor.increment_progress();
		}

		private void _handle_jobject(
			string file,
			Newtonsoft.Json.Linq.JObject obj,
			ConcurrentDictionary<string, classes.prefab.SpriteCollection> sprite_collections,
			ConcurrentDictionary<string, classes.prefab.ClipCollection> clip_collections
		) {
			// What are we dealing with?
			if (obj.GetValue("MonoBehaviour") is Newtonsoft.Json.Linq.JObject monobehavior) {
				// What kind of monobehavior is it?
				var meta_file = $"{file}.meta";

				if (monobehavior.Property("spriteDefinitions") != null) {
					// It's a sprite collection.
					var sprite_collection = monobehavior.ToObject<classes.prefab.SpriteCollection>();
					if (sprite_collection == null) {
						throw new ApplicationException($"Could not extract sprite collection from {file}.");
					}

					if (monobehavior.Property("textures") == null) {
						throw new ApplicationException("Has sprite defs but no textures??");
					}

					sprite_collection.guid = AssetExtractor.path_to_guid_meta_map[meta_file];

					log.Info("{0} is a sprite collection.", sprite_collection.name);

					if (!sprite_collections.TryAdd(sprite_collection.guid, sprite_collection)) {
						throw new ApplicationException($"GUID conflict in sprite collections.");
					}
					// this._sprite_collection_guids.Add(sprite_collection.guid, this._sprite_collections.Count);
				}
				else if (monobehavior.Property("clips") != null) {
					// It's a clip collection.
					var clip_collection = monobehavior.ToObject<classes.prefab.ClipCollection>();
					if (clip_collection == null) {
						throw new ApplicationException($"Could not extract clip collection from {file}.");
					}

					clip_collection.guid = AssetExtractor.path_to_guid_meta_map[meta_file];


					// this._clip_collections.Add(clip_collection);
					// // We must also find a clip name ourselves.
					// this._clip_collection_guid_to_names.Add(clip_collection.guid, System.IO.Path.GetFileNameWithoutExtension(file));
					if (!clip_collections.TryAdd(
						System.IO.Path.GetFileNameWithoutExtension(file),
						clip_collection
					)) {
						throw new ApplicationException($"GUID conflict in clip collections.");
					}

					log.Info("{0} is a clip collection.", System.IO.Path.GetFileNameWithoutExtension(file));

				}
				else {
					// throw new ApplicationException($"Unknown and unimplemented prefab file at {file}.");
					// Just do nothing. The majority we do not care about.
				}
			}

		}

		private void _handle_sprite_collection(classes.prefab.SpriteCollection sprite_collection) {
			// Note: do not track progress of each sprite as it
			// becomes less helpful in tracking progress.
			log.Info("Handling sprite collection {0}.", sprite_collection.name);

			if (sprite_collection.textures.Length < 1) {
				// Seems to happen with Fungus.prefab only.
				// It seems to also not have any sprite definitions.
				log.Info("Skipping sprite collection `{0}` because it has no textures. No implementation for this.", sprite_collection.name);
				AssetExtractor.increment_progress();
				return;
			}

			Debug.Assert(sprite_collection.textures.Length == sprite_collection.materials.Length);

			var material_texture_map = new Dictionary<string, string>();
			for (int i = 0; i < sprite_collection.materials.Length; i++) {
				material_texture_map.Add(sprite_collection.materials[i].guid, sprite_collection.textures[i].guid);
			}
			Debug.Assert(material_texture_map.Count == sprite_collection.textures.Length);

			// Load all the atlases first.
			var atlas_images = new Dictionary<string, System.Drawing.Bitmap>();
			for (int i = 0; i < sprite_collection.textures.Length; i++) {
				var guid = sprite_collection.textures[i].guid;
				var atlas_image = new System.Drawing.Bitmap(AssetExtractor.guids[guid].child_path);
				atlas_images.Add(guid, atlas_image);
			}

			// And then split the atlases into sprites.
			for (int i = 0; i < sprite_collection.sprite_definitions.Length; i++) {
				var sprite_def = sprite_collection.sprite_definitions[i];
				if (sprite_def.name == null) {
					// We *could* give it a fake name like this:
					// `sprite_def.name = "${sprite_collection.name}___{i}";`
					// However, from observation, the result of these null images
					// seem to always be the same.
					// Specifically, it seems to be a few pixels without any
					// useful information.
					// We also won't warn the user about this; there are a
					// *lot* of these.
					continue;
				}

				if (sprite_def.uvs.Length < 1) {
					log.Warn("Skipping sprite collection definition {0} / {1} with no UVs.", sprite_collection.name, sprite_def.name);
					continue;
				}

				// Let's find which texture this sprite has.
				var atlas_image = atlas_images[material_texture_map[sprite_def.material.guid]];
				// var atlas_image_size = atlas_image.GetSize();
				var atlas_image_size = new Vector2(atlas_image.Width, atlas_image.Height);

				var (uv_smallest, uv_largest) = new HallowsimExtractor.util.Extreme(sprite_def.uvs).get();
				uv_smallest = (uv_smallest * atlas_image_size).Round();
				uv_largest = (uv_largest * atlas_image_size).Round();

				const int OFFSET = 1;

				var uv_region = new Rect2(
					uv_smallest.x,
					// Yeah, this is kind of odd, but it does work.
					// We need to also subtract the height,
					// immediately after.
					atlas_image_size.y - uv_smallest.y,
					uv_largest - uv_smallest
				);
				uv_region.Size += new Vector2(OFFSET, OFFSET);
				uv_region.Position -= new Vector2(0, uv_region.Size.y);

				if (uv_region.Position.x <= 0 || uv_region.Position.y <= 0) {
					log.Info("Skipping sprite collection definition {0} / {1} due to it having negative UV positions.", sprite_collection.name, sprite_def.name);
					this._negative_sprites.Add($"{sprite_collection.name}/{sprite_def.name}");
					continue;
				}

				using (var sprite_image = new System.Drawing.Bitmap((int)uv_region.Size.x, (int)uv_region.Size.y, atlas_image.PixelFormat)) {
					using (var g = System.Drawing.Graphics.FromImage(sprite_image)) {
						g.DrawImage(
							atlas_image,
							new System.Drawing.Rectangle(0, 0, sprite_image.Width, sprite_image.Height),
							new System.Drawing.Rectangle((int)uv_region.Position.x, (int)uv_region.Position.y, (int)uv_region.Size.x, (int)uv_region.Size.y),
							System.Drawing.GraphicsUnit.Pixel
						);
					}

					if (sprite_def.flipped == 1) {
						sprite_image.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipY);
					}

					var dir = System.IO.Path.Combine(
						AssetExtractor.output_dir,
						"images",
						"collections",
						sprite_collection.name
					);

					System.IO.Directory.CreateDirectory(dir);
					sprite_image.Save(System.IO.Path.Combine(dir, $"{sprite_def.name}.png"));
				}
			}

			// We can forget these now.
			foreach (var atlas_image in atlas_images.Values) {
				atlas_image.Dispose();
			}
			atlas_images.Clear();
			material_texture_map.Clear();

			AssetExtractor.increment_progress();

		}

		private void _handle_clip_collection(classes.prefab.ClipCollection clip_collection) {
			log.Info("Handling clip collection {0}", this._clip_collection_guid_to_names[clip_collection.guid]);

			var dir = System.IO.Path.Combine(
				AssetExtractor.output_dir,
				"images",
				"clips",
				// the clip collection name
				this._clip_collection_guid_to_names[clip_collection.guid]
			);

			for (int i = 0; i < clip_collection.clips.Length; i++) {
				var clip = clip_collection.clips[i];
				this._handle_clip(clip, clip_collection, dir, i);
			}

			AssetExtractor.increment_progress();

		}

		private void _handle_clip(classes.prefab.Clip clip, classes.prefab.ClipCollection clip_collection, string dir, int index) {
			var clip_collection_name = this._clip_collection_guid_to_names[clip_collection.guid];
			if (clip.name == null) {
				// Not anything to worry about.
				log.Info("Skipping null clip {0} at index {1}.", clip_collection_name, index);
				return;
			}

			if (clip.frames.Length < 1) {
				// Also nothing to worry about.
				log.Info("Skipping empty clip {0} / {1}.", clip_collection_name, clip.name);
				return;
			}

			var new_clip = new classes.prefab.ConvertedClip() {
				name = clip.name,
				frames = new string[clip.frames.Length],
				fps = clip.fps,
				loop_start = clip.loop_start,
				wrap_mode = clip.wrap_mode
			};

			var clip_pos_extreme = new HallowsimExtractor.util.Extreme();

			for (int i = 0; i < clip.frames.Length; i++) {
				var frame = clip.frames[i];

				var parent_sprite_collection = this._sprite_collections[frame.sprite_collection.guid];
				classes.prefab.SpriteDefinition parent_sprite_definition;
				try {
					parent_sprite_definition = parent_sprite_collection.sprite_definitions[frame.sprite_id];
				}
				catch (IndexOutOfRangeException e) {
					if (frame.sprite_id >= parent_sprite_collection.sprite_definitions.Length) {
						log.Info(
							"Skipping clip ({0} / {1} frame id {2}), " +
							"because it has a frame with invalid sprite id ({3}). The " +
							"request sprite collection only has " +
							"{4} definitions, but " +
							"this clip tried to get something larger than that count.",
							clip_collection_name,
							new_clip.name,
							i,
							frame.sprite_id,
							parent_sprite_collection.sprite_definitions.Length
						);
						return;
					}
					else {
						log.Error("Error with index in clip.");
						log.Error("Clip: {0} / {1} frame id {2}", clip_collection_name, new_clip.name, i);
						log.Error("Parent sprite collection: {0}", parent_sprite_collection.name);
						log.Error("Tried to get sprite def index {0}. Sprite id is {1}. Count is {2}", frame.sprite_id, frame.sprite_id, parent_sprite_collection.sprite_definitions.Length);
						log.Error("Error: {0}", e);
						throw;
					}

				}

				if (parent_sprite_definition.name == null || parent_sprite_definition.name.Length < 1) {
					log.Info("In clip {0}, sprite definition at index {1} has empty name. Skipping clip.", clip.name, i);
					return;
				}

				if (this._negative_sprites.Contains($"{parent_sprite_collection.name}/{parent_sprite_definition.name}")) {
					// It's a sprite with a negative UV region, so we should skip it.
					continue;
				}

				// Use vanilla domain.
				new_clip.frames[i] = $"vanilla:clips/{clip_collection_name}/{clip.name}/{parent_sprite_definition.name}";

				// While we are in this loop, we can also find the largest image in the clip.
				clip_pos_extreme.find(parent_sprite_definition.positions);
			}

			// Now we can recreate images with proper positioning.
			var image_dir = System.IO.Path.Combine(dir, new_clip.name);
			System.IO.Directory.CreateDirectory(image_dir);

			for (int i = 0; i < clip.frames.Length; i++) {
				var frame = clip.frames[i];

				var frame_pos_extreme = new HallowsimExtractor.util.Extreme();

				// No need for error checking here; it is already done above.
				var parent_sprite_collection = this._sprite_collections[frame.sprite_collection.guid];
				var parent_sprite_definition = parent_sprite_collection.sprite_definitions[frame.sprite_id];

				frame_pos_extreme.find(parent_sprite_definition.positions);

				const int OFFSET = 1;

				if (this._negative_sprites.Contains($"{parent_sprite_collection.name}/{parent_sprite_definition.name}")) {
					// It's a negative sprite; skip it.
					continue;
				}

				using (var orig_image = new System.Drawing.Bitmap(System.IO.Path.Combine(AssetExtractor.output_dir, "images", "collections", parent_sprite_collection.name, $"{parent_sprite_definition.name}.png"))) {
					using (var larger_image = new System.Drawing.Bitmap(
						(int)((clip_pos_extreme.largest.x - clip_pos_extreme.smallest.x) / parent_sprite_definition.texel_size.x + OFFSET),
						(int)((clip_pos_extreme.largest.y - clip_pos_extreme.smallest.y) / parent_sprite_definition.texel_size.y + OFFSET),
						orig_image.PixelFormat
					)) {
						using (var g = System.Drawing.Graphics.FromImage(larger_image)) {
							g.DrawImageUnscaled(

								orig_image,
								new System.Drawing.Point(
									(int)((frame_pos_extreme.smallest.x - clip_pos_extreme.smallest.x) / parent_sprite_definition.texel_size.x),
									(int)((frame_pos_extreme.smallest.y - clip_pos_extreme.smallest.y) / parent_sprite_definition.texel_size.y)
								)
							);

						}

						larger_image.Save(System.IO.Path.Combine(dir, clip.name, $"{parent_sprite_definition.name}.png"));

					}
				}


			}

			// Now we can write the JSON.
			// TODO: We could do it async.
			System.IO.Directory.CreateDirectory(dir);
			System.IO.File.WriteAllText(
				System.IO.Path.Combine(dir, $"{new_clip.name}.json"),
				Newtonsoft.Json.JsonConvert.SerializeObject(
					new_clip,
					Newtonsoft.Json.Formatting.Indented
				)
			);
		}
	}
}
