namespace HallowsimExtractor.extraction.directoryhandler {
	internal class AudioClipDirectoryHandler : util.CopyHandler
	{

		internal override string name { get { return "AudioClip"; } }

		internal override string destination_name { get { return "audio"; } }
	}
}
