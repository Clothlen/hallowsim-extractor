namespace HallowsimExtractor {
	using System.Collections.Generic;

	internal static class YAMLHelper
	{

		internal static string clean_yaml(string[] lines) {

			var new_lines = new List<string>(lines);
			for (int i = new_lines.Count - 1; i >= 0; i--) {
				var line = new_lines[i];

				// Don't care about document starts.
				if (line.StartsWith("---")) {
					new_lines.RemoveAt(i);
				}
				// We don't care about directives, either.
				else if (line.StartsWith("%")) {
					new_lines.RemoveAt(i);
				}

			}

			return string.Join("\n", new_lines);

		}
	}
}
