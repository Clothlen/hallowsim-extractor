namespace HallowsimExtractor {
	using System;

	public static class Core
	{

		private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

		static Core() {
			_setup_log();
			log.Info("Created log.");
			AppDomain.CurrentDomain.UnhandledException += Core._unhandled_exception;
		}

		// This "wakes up" the Core and starts the static constructor.
		internal static void poke() { }

		private static void _setup_log() {
			var config = new NLog.Config.LoggingConfiguration();
			var logpath = Godot.ProjectSettings.GlobalizePath("user://logs/core-log.txt");

			var layout = _layout;

			var log_console = new NLog.Targets.ConsoleTarget("log_console") {
				Layout = layout
			};
			var log_file = new NLog.Targets.FileTarget() {
				Layout = layout,
				FileName = logpath,
				LineEnding = NLog.Targets.LineEndingMode.LF,
				CreateDirs = true,
				FileAttributes = NLog.Targets.Win32FileAttributes.ReadOnly,
				KeepFileOpen = true,
				ArchiveFileName = _filename_layout,
				ArchiveNumbering = NLog.Targets.ArchiveNumberingMode.Sequence,
				ArchiveEvery = NLog.Targets.FileArchivePeriod.Day,
				ArchiveOldFileOnStartup = true,
				MaxArchiveFiles = 100
			};

			config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, log_console);
			config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, log_file);

			// Only print to Godot editor console if we are in the Godot
			// editor.
#if TOOLS
			var log_gd = new _GDTarget() {
				Layout = layout
			};
			config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, log_gd);
#endif

			NLog.LogManager.Configuration = config;

		}

		private static NLog.Layouts.Layout _layout {
			get {
				const string str = @"${date:format=yyyy-MM-dd HH\:mm\:ss} [${level}/${logger}/${threadid}] ${message}  ${exception}";
				return NLog.Layouts.SimpleLayout.FromString(
					str,
					throwConfigExceptions: true
				);
			}
		}

		private static NLog.Layouts.Layout _filename_layout {
			get {
				const string str = @"core-log_${date:format=yyyy-MM-dd}_{#}.txt";
				return NLog.Layouts.SimpleLayout.FromString(
					Godot.ProjectSettings.GlobalizePath($"user://logs/{str}"),
					throwConfigExceptions: true
				);
			}
		}

		private static void _unhandled_exception(object sender, UnhandledExceptionEventArgs e) {
			log.Error("Unhandled exception from {0}: {1}", sender, e);
		}

		private sealed class _GDTarget : NLog.Targets.TargetWithLayout
		{
			// Only used for Godot editor.
			protected override void Write(NLog.LogEventInfo log_event)
			{
				var message = this.Layout.Render(log_event);

				switch (log_event.Level.Ordinal) {
					case int n when n >= NLog.LogLevel.Error.Ordinal:
						Godot.GD.PushError(message);
						break;
					case int n when n >= NLog.LogLevel.Warn.Ordinal:
						Godot.GD.PushWarning(message);
						break;
				}

				Godot.GD.Print(message);

			}
		}

	}
}
