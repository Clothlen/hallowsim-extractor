namespace HallowsimExtractor.scenes {
	using Godot;

	public class CoreNode : Node
	{

		public override void _Ready() {
			Core.poke();
		}

		public override void _UnhandledKeyInput(InputEventKey evt) {
			if (evt.IsActionPressed("quit")) {
				this.GetTree().Quit();
			}
		}
	}
}
