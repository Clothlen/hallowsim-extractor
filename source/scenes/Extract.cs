namespace HallowsimExtractor.scenes {
	using Godot;

	public class Extract : Node2D
	{

		private const float PROGRESS_POLL_TIME = 0.25f;

		private Node2D _progress;
		private Label _progress_label;
		private Node2D _spinner;

		public override void _Ready() {
			extraction.AssetExtractor.go();

			this._progress = this.GetNode<Node2D>("Progress");
			this._progress_label = this._progress.GetNode<Label>("Label");

			var timer = new Timer();
			timer.Connect("timeout", this, nameof(this._on_timer));
			this.AddChild(timer);
			timer.WaitTime = PROGRESS_POLL_TIME;
			timer.Start();
			// Call once immediately.
			this._on_timer();

			this._spinner = this.GetNode<Node2D>("Spinner");

		}

		public override void _PhysicsProcess(float delta) {
			// Spin the spinner so we know things are happening.

			this._spinner.Rotate(Mathf.Deg2Rad(1));
		}

		private void _on_timer() {
			var (resets, progress, max_progress) = extraction.AssetExtractor.poll_progress();
			string percentage;
			if (max_progress == 0) {
				percentage = $"...%";
			}
			else {
				// this._progress_label.Text = $"{progress}/{max_progress} ({(progress/max_progress).ToString("P", System.Globalization.CultureInfo.InvariantCulture)})";
				percentage = $"{((float)progress / (float)max_progress * 100f):F}%";
			}

			this._progress_label.Text = $"Stage {resets}\n{progress}/{max_progress} ({percentage})";

		}
	}
}
