namespace HallowsimExtractor.util {
	using System.Diagnostics;
	public static class ArrayUtil
	{
		public static byte[] rotate_rgba_90(byte[] array, int width, int height) {

			var new_array = new byte[array.Length];

			Debug.Assert(width * height * 4 == array.Length, $"w*h should == a.l | width: {width} * height: {height} = {width * height} | array.length: {array.Length}");

			for (int y = 0, y2 = height - 1; y < height; ++y, --y2) {
				var offset = y * width;

				for (int x = 0; x < width; x++) {

					for (int i = 0; i < 4; i++) {
						new_array[(x * height + y2) * 4 + i] = array[(offset + x) * 4 + i];
					}

				}

			}

			return new_array;

		}
	}
}
