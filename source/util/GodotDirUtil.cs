namespace HallowsimExtractor.util {
	using System.Collections.Immutable;
	using System.Collections.Generic;

	public static class GodotDirUtil
	{

		public static bool dir_exists(string path) {
			var dir = new Godot.Directory();
			var returner = dir.DirExists(path);
			dir.Dispose();
			return returner;
		}

		public static ImmutableList<string> list_dir(string path) {
			var dir = new Godot.Directory();

			dir.Open(path);

			var result = new List<string>();
			dir.ListDirBegin();

			while (true) {
				var file = dir.GetNext();
				if (file == "") {
					break;
				}

				if (file == "." || file == "..") {
					continue;
				}

				result.Add($"{path}/{file}");
			}

			dir.ListDirEnd();

			dir.Dispose();

			return result.ToImmutableList();

		}

		public static ImmutableList<string> list_only_dirs(string path) {
			var dir = new Godot.Directory();

			dir.Open(path);

			var result = new List<string>();
			dir.ListDirBegin();

			while (true) {
				var file = dir.GetNext();
				if (file == "") {
					break;
				}

				if (file == "." || file == "..") {
					continue;
				}

				if (!dir.CurrentIsDir()) {
					continue;
				}

				result.Add($"{path}/{file}");
			}

			dir.ListDirEnd();

			dir.Dispose();

			return result.ToImmutableList();
		}

		public static ImmutableList<string> list_only_files(string path) {
			var dir = new Godot.Directory();

			dir.Open(path);

			var result = new List<string>();
			dir.ListDirBegin();

			while (true) {
				var file = dir.GetNext();
				if (file == "") {
					break;
				}

				if (file == "." || file == "..") {
					continue;
				}

				if (dir.CurrentIsDir()) {
					continue;
				}

				result.Add($"{path}/{file}");
			}

			dir.ListDirEnd();

			dir.Dispose();

			return result.ToImmutableList();
		}

		public static ImmutableList<string> list_dir_recursive(string path) {
			var dir = new Godot.Directory();

			dir.Open(path);

			dir.ListDirBegin();

			var result = new List<string>();

			while (true) {
				var file = dir.GetNext();
				if (file == "") {
					break;
				}

				if (file == "." || file == "..") {
					continue;
				}

				if (dir.CurrentIsDir()) {
					// yeah, file is dir
					result.AddRange(list_dir_recursive($"{path}/{file}"));
				}
				else {
					result.Add($"{path}/{file}");
				}
			}

			dir.ListDirEnd();

			dir.Dispose();

			return result.ToImmutableList();

		}

		public static void make_dir_recursive(string path) {
			var dir = new Godot.Directory();
			try {
				dir.MakeDirRecursive(path);
			}
			finally {
				dir.Dispose();
			}
		}

		public static void copy(string from, string to) {
			var dir = new Godot.Directory();
			try {
				dir.Copy(from, to);
			}
			finally {
				dir.Dispose();
			}
		}

	}
}
