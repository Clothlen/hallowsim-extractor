namespace HallowsimExtractor.util {
	using System;
	using System.Collections.Generic;

	internal static class GodotFileUtil
	{

		internal static bool file_exists(string path) {
			var f = new Godot.File();

			if (f.FileExists(path)) {
				f.Dispose();
				return true;
			}

			f.Dispose();
			return false;
		}

		internal static string read_file(string path) {
			// var f = new Godot.File();

			// string returner;
			// try {
			// 	f.Open(path, Godot.File.ModeFlags.Read);

			// 	try {
			// 		returner = f.GetAsText();
			// 	}
			// 	finally {
			// 		f.Close();
			// 	}
			// }
			// finally {
			// 	f.Dispose();
			// }

			// string returner;
			// using (var f = new Godot.File()) {
			// 	f.Open(path, Godot.File.ModeFlags.Read);

			// 	try {
			// 		returner = f.GetAsText();
			// 	}
			// 	finally {
			// 		f.Close();
			// 	}

			// }

			var f = new Godot.File();
			string returner;
			f.Open(path, Godot.File.ModeFlags.Read);

			try {
				returner = f.GetAsText();
			}
			finally {
				f.Close();
			}

			if (returner == null) {
				throw new ApplicationException("Error reading Godot file.");
			}
			return returner;

		}

		internal static void write_file(string path, string text) {
			var f = new Godot.File();

			try {
				f.Open(path, Godot.File.ModeFlags.Write);
				try {
					f.StoreString(text);
				}
				finally {
					f.Close();
				}
			}
			finally {
				f.Dispose();
			}
		}

	}
}
