namespace HallowsimExtractor.util {
	using Godot;

	public class Extreme
	{

		public Vector2 smallest {
			get {
				return this._smallest;
			}
		}

		public Vector2 largest {
			get {
				return this._largest;
			}
		}

		// public Vector2 difference {
		// 	get {
		// 		return this._largest - this._smallest;
		// 	}
		// }

		private Vector2 _largest = new Vector2(float.MinValue, float.MinValue);

		private Vector2 _smallest = new Vector2(float.MaxValue, float.MaxValue);

		public Extreme() { }

		public Extreme(Vector2[] vectors) {
			this.find(vectors);
		}

		public Extreme(Vector3[] vectors) {
			this.find(vectors);
		}

		public void find(Vector2[] vectors) {
			foreach (var vec in vectors) {
				if (vec.x > this._largest.x) {
					this._largest.x = vec.x;
				}
				if (vec.y > this._largest.y) {
					this._largest.y = vec.y;
				}

				if (vec.x < this._smallest.x) {
					this._smallest.x = vec.x;
				}
				else if (vec.y < this._smallest.y) {
					this._smallest.y = vec.y;
				}

			}
		}

		public void find(Vector3[] vectors) {
			// Drop the z.
			var vec2s = new Vector2[vectors.Length];

			for (int i = 0; i < vectors.Length; i++) {
				vec2s[i] = new Vector2(vectors[i].x, vectors[i].y);
			}

			this.find(vec2s);

		}

		public (Vector2 smallest, Vector2 largest) get() {
			return (this._smallest, this._largest);
		}

		public override string ToString()
		{
			return $"{this.GetType().FullName}: {this.largest}, {this.smallest}";
		}

	}
}
