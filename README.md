# Hallowsim Extractor

This program is used to extract assets from Hollow Knight and put it into a more usable format.

uTinyRipper does the heavy work of actually extracting the assets, while this program then converts
it into something more usable.

## Why

Hollow Knight has a vast variety of assets. Extracting them with a tool is not difficult, but no tools
that I have found account for things such as 2DToolkit's sprite atlases. This tool programmatically handles
those things.

## Usage

### uTinyRipper

Download uTinyRipper from [here (direct download, ~2.1 MB)](https://downloads.sourceforge.net/project/utinyripper/2020-04-13/10-02-02/uTinyRipper_x64.zip?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Futinyripper%2Ffiles%2Flatest%2Fdownload&ts=1598035358).

Extract its zip file; it doesn't matter to where. Run `uTinyRipper.exe`.

A window should pop up that shows a "Drag and Drop" area.

Next, find your Hollow Knight common folder. This depends heavily on where you have Hollow Knight installed.
On Steam on Windows devices, it will likely be at:

`C:\Program Files (x86)\Steam\steamapps\common\Hollow Knight`

However, if you installed it to a different location or on another device, you will have to find it yourself.

Inside of that folder should be another folder, named `hollow_knight_Data`. Drag this folder to uTinyRipper.

On the bottom of the window, it should display some loading information. Wait for it to say that all files
have been loaded.

Click `Export`. Choose any folder to export to. It will create a folder underneath that named `globalgamemanagers`.

Wait for it to export. This may take a long time; expect 10 minutes to an hour depending upon your computer.
Maybe play some Hollow Knight while you wait.

### Using Hallowsim Extractor

Find the folder that you exported to with uTinyRipper. Copy it to Hallowsim Extractor's data folder. The
location of it depends on your operating system:

Windows: `C:\Users\yourname\AppData\Roaming\Hallowsim Extractor`
MacOS: `~/Library/Application Support/Hallowsim Extractor`
Linux: `~/.config/Hallowsim Extractor`

Note that this tool has only been tested on Windows.

Make sure that the structure is set up so that the application folder contains `globalgamemanagers`.

Finally, open Hallowsim Extractor by running its executable file. It will immediately begin
the conversion if you did everything right.

It will finish after about 5 to 15 minutes, depending on your computer.

When it is done, the progress bar should say 0/0.

When it is done, open the data folder again. Within, there should be a folder named `vanilla`.

Copy this to Hallowsim's mod folder; it should look like this if you do it correctly:

Windows: `C:\Users\yourname\AppData\Roaming\Hallowsim\mods\vanilla`
MacOS: `~/Library/Application Support/Hallowsim/mods/vanilla`
Linux: `~/.config/Hallowsim/mods/vanilla`

Now, you can finally open Hallowsim.

## Structure

This tool creates a folder named `vanilla`. Within it, it contains three folders full of the things
that were wanted to be extracted.

The first folder is `audio`, which contains all audio in the game in `.wav` format.

The second folder is `font`, which contains the fonts used by the game.

The third folder is `images`. This folder contains all textures in the game, organized by type.

### Images

Inside of `vanilla/images` are three more folders.

The first folder is `clips`. This contains a number of "categories." Each category itself contains a number of
`.json` files. Each JSON file is structured like this:

```json
{
  "name": "Clip Name",
  "frames": [
    "modid:Collection name/frame name1",
    "modid:Collection name/frame name2"
  ],
  "fps": 10.0,
  "loop_start": 0,
  "wrap_mode": 2
}
```

The `name` field is the name of the clip. It is usually (always?) the same as the file name, but without the
extension.

The `frames` field is an array of strings. Each string is formatted like `<modid>:<collection name>/<image name>`.
The modid is always `vanilla` and can be ignored if you are using this for your own project. The collection name
refers to a folder inside the collections directory (see below). The image name refers to an image within a
collection, without the extension.

The `fps` field is the frames per second. (This may or may not be accurate.)

The `loop_start` field refers to the starting frame for loops. This only applies if the `wrap_mode` is set to
LOOP_PART.

The `wrap_mode` refers to how looping works. Refer to this:

```js
// Loops forever.
LOOP = 0
// Starts from beginning, and loops forever.
// However, loops start from `loop_start` field.
LOOP_PART = 1
// Does not loop.
ONCE = 2
// Loops reverse direction each time the animation finishes.
PING_PONG = 3
// Instead of looping, a random frame is chosen.
RANDOM_FRAME = 4
// Not sure about 5
// Looping does not apply as there is only 1 frame.
SINGLE = 6
```

The second folder is `collections`. This folder contains a number of other folders. Each of these
folders contains a number of images stored in PNG format.

The third folder is `sprites`. This folder contains a number of images stored in PNG format. They do
not belong to any particular category.

Some images in the sprites folder seem to actually belong to a collection. However, there does not seem
to be a way to programmatically collect these together, except perhaps by comparing filenames.

## Credits

This project is licensed under the MIT license.

This uses the [Mono Debugger Enabler by Duroxxigar](https://godotengine.org/asset-library/asset/435)
under the MIT license.

This uses [JSON.NET](https://www.newtonsoft.com/json) under the MIT license.

This uses [YamlDotNet](https://github.com/aaubry/YamlDotNet) under the MIT license.

This uses [Godot](https://godotengine.org/) under the MIT license.

This uses [MessagePack](https://github.com/neuecc/MessagePack-CSharp) under the MIT license.
